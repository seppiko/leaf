# Seppiko Leaf

![GitLab](https://img.shields.io/gitlab/license/seppiko/leaf?color=blue&style=flat-square)
![GitLab tag (latest by SemVer)](https://img.shields.io/gitlab/v/tag/seppiko/leaf?style=flat-square)

## Introduction

A network service for generating Snowflake IDs at high scale with
some simple guarantees.

[https://github.com/twitter/snowflake](https://github.com/twitter/snowflake)

Algorithm implement move to [Seppiko Snowflake](https://gitlab.com/seppiko/snowflake)

If you need RESTful API. Please see [Leaf-Gateway](https://gitlab.com/seppiko/leaf-gateway)

## How to start

You need to modify `config.json` for configure leaf server.

Use `-Dleaf.configFile=config.json` custom `config.json` file path.

## Configuration

### `config.json`

```json
{
  "host": "0.0.0.0",
  "port": 9800,
  "timeOffset": 1577420144
}
```

`timeOffset` is an initial time offset, you can set it on you first time (Millisecond).
You can use `example\Generator.java` generator it.

## Compile and package

```shell script
mvn clean
mvn protobuf:compile
mvn protobuf:compile-custom
mvn package
```

## Example

This is gRPC service, please see `example\Client.java` for you.

Before you use Leaf Client, **please copy `src\main\proto\leaf.proto` to you project.**
And run `mvn protobuf:compile protobuf:compile-custom` for generator related classes.

## Sponsors

<a href="https://www.jetbrains.com/" target="_blank"><img src="https://seppiko.org/images/jetbrains.png" alt="JetBrians" width="100px"></a>
