/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.leaf.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.Reader;
import java.util.List;

/**
 * JSON Util with GSON
 *
 * @author Leonard Woo
 */
public class JsonUtil {

  private static final Gson gson;

  static {
    gson = new GsonBuilder()
        .create();
  }

  /**
   * To JSON string
   *
   * @param t Object
   * @return JSON string
   */
  public static <T> String toJson(T t) {
    return gson.toJson(t);
  }

  /**
   * From JSON to Object
   *
   * @param json JSON string
   * @param objectType Object type
   * @return Object
   */
  public static <T> T fromJson(String json, Class<T> objectType) {
    return gson.fromJson(json, objectType);
  }

  /**
   * From JSON to Object
   *
   * @param reader JSON Reader
   * @param objectType Object type
   * @return Object
   */
  public static <T> T fromJson(Reader reader, Class<T> objectType) {
    return gson.fromJson(reader, objectType);
  }

  /**
   * Get a value in JSON string
   *
   * @param json JSON string
   * @param key JSON key
   * @return Value
   */
  public static String fromJsonToString(String json, String key) {
    return fromJsonObject(json).get(key).getAsString();
  }

  /**
   * Parse JSON string to JSON Object
   *
   * @param json JSON string
   * @return JSON Object
   */
  public static JsonObject fromJsonObject(String json) {
    return JsonParser.parseString(json).getAsJsonObject();
  }

  /**
   * Object to JSON Object
   *
   * @param t Object
   * @return JSON Object
   */
  public static <T> JsonObject toJsonObject(T t) {
    return gson.toJsonTree(t).getAsJsonObject();
  }

  /**
   * List to JSON Element
   *
   * @param list List
   * @return JSON Element
   */
  public static <T> JsonElement toJsonElement(List<T> list) {
    return gson.toJsonTree(list);
  }

}
