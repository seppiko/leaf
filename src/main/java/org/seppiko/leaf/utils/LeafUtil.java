/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.leaf.utils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintStream;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.seppiko.commons.utils.StringUtil;
import org.seppiko.glf.api.Logger;
import org.seppiko.glf.api.LoggerFactory;

/**
 * Leaf utility
 *
 * @author Leonard Woo
 */
public class LeafUtil {

  private static final Logger logger = LoggerFactory.getLogger(LeafUtil.class);

  private static final LeafUtil INSTANCE = new LeafUtil();

  /** Singleton instance */
  public static LeafUtil getInstance() {
    return INSTANCE;
  }

  private static final String[] BANNER = {"",
      "  ____                   _ _           _                __ ",
      " / ___|  ___ _ __  _ __ (_) | _____   | |    ___  __ _ / _|",
      " \\___ \\ / _ \\ '_ \\| '_ \\| | |/ / _ \\  | |   / _ \\/ _` | |_ ",
      "  ___) |  __/ |_) | |_) | |   < (_) | | |__|  __/ (_| |  _|",
      " |____/ \\___| .__/| .__/|_|_|\\_\\___/  |_____\\___|\\__,_|_|  ",
      "            |_|   |_|                                      "
  };
  private static final String PROJECT_NAME = " :: Seppiko Leaf ::";

  private static final String groupId = "org.seppiko";
  private static final String artifactId = "leaf";
  private String version;

  /** print banner */
  public void printBanner(PrintStream out) {
    try {
      Element root = parser(getPomInputStream());
      this.version = root.elementText("version");
      String version = !StringUtil.isNullOrEmpty(this.version)? " (v" + this.version + ")": "";

      int maxLength = 0;
      for (String s : BANNER) {
        maxLength = Math.max(maxLength, s.length());
        out.println(s);
      }

      out.print(PROJECT_NAME);
      if (!version.isEmpty()) {
        String padding = String.valueOf(' ')
            .repeat((maxLength - (version.length() + PROJECT_NAME.length())));
        out.print(padding + version);
      }
      out.println();
      out.flush();
    } catch (Exception ex) {
      logger.warn("BANNER load failed.", ex);
    }
  }

  /** @return version string */
  public String getVersion() {
    return version;
  }

  private Element parser(InputStream is) throws DocumentException {
    SAXReader reader = new SAXReader();
    return reader.read(is).getRootElement();
  }

  private InputStream getPomInputStream() throws FileNotFoundException {
    InputStream is = LeafUtil.class.getResourceAsStream(
        String.format("/META-INF/maven/%s/%s/pom.xml", groupId, artifactId) );
    if(is == null) {
      is = new FileInputStream("pom.xml");
    }
    return new BufferedInputStream(is);
  }
}
