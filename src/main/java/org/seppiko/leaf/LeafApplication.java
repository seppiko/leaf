/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.leaf;

import io.grpc.Server;
import io.grpc.netty.shaded.io.grpc.netty.NettyServerBuilder;
import java.io.IOException;
import java.net.InetSocketAddress;
import org.seppiko.commons.utils.StringUtil;
import org.seppiko.glf.api.Logger;
import org.seppiko.glf.api.LoggerFactory;
import org.seppiko.leaf.config.LeafConfiguration;
import org.seppiko.leaf.entity.LeafConfigEntity;
import org.seppiko.leaf.utils.LeafUtil;

/**
 * Seppiko Leaf
 *
 * @author Leonard Woo
 */
public class LeafApplication {

  private static final Logger logger = LoggerFactory.getLogger(LeafApplication.class);

  public static void main(String[] args) throws InterruptedException, IOException {
    LeafUtil.getInstance().printBanner(System.out);

    LeafConfigEntity config = LeafConfiguration.getInstance().getConfig();
    String host = config.host();
    int port = config.port();

    NettyServerBuilder build;
    if (StringUtil.isNullOrEmpty(host) || LeafConfiguration.HTTP_DEFAULT_HOSTNAME.equals(host)) {
      build = NettyServerBuilder.forPort(port);
      host = LeafConfiguration.HTTP_DEFAULT_HOSTNAME;
    } else {
      build = NettyServerBuilder.forAddress(new InetSocketAddress(host, port));
    }

    Server server = build.addService(new LeafServiceImpl(config.timeOffset()))
        .build();
    logger.info(String.format("Service stating ... [%s:%d]", host, port));
    server.start();

    Runtime.getRuntime().addShutdownHook(new Thread(server::shutdown));
    server.awaitTermination();
  }
}
