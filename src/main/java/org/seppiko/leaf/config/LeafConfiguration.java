/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.leaf.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import org.seppiko.commons.utils.Environment;
import org.seppiko.commons.utils.IOStreamUtil;
import org.seppiko.glf.api.Logger;
import org.seppiko.glf.api.LoggerFactory;
import org.seppiko.leaf.entity.LeafConfigEntity;
import org.seppiko.leaf.utils.JsonUtil;


/**
 * Leaf Configuration
 *
 * @author Leonard Woo
 */
public class LeafConfiguration {

  private static final LeafConfiguration INSTANCE = new LeafConfiguration();

  /** Singleton instance */
  public static LeafConfiguration getInstance() {
    return INSTANCE;
  }

  private static final Logger logger = LoggerFactory.getLogger(LeafConfiguration.class);

  public static final String HTTP_DEFAULT_HOSTNAME = "0.0.0.0";

  private LeafConfigEntity config;

  private LeafConfiguration() {
    try {
      String filepath = System.getProperty("leaf." + Environment.CONFIG_FILE_PARAMETER_SUFFIX, Environment.CONFIG_FILENAME_JSON);
      InputStream is = IOStreamUtil.getStream(new File(filepath) );
      if(Objects.isNull(is)) {
        throw new FileNotFoundException();
      }
      BufferedReader reader = IOStreamUtil.loadReader( is );
      config = JsonUtil.fromJson(reader, LeafConfigEntity.class);
    } catch (IOException | RuntimeException ex) {
      logger.error("Config file not found", ex);
    }
  }

  public LeafConfigEntity getConfig() {
    return config;
  }
}
