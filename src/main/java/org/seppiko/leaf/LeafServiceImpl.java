/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.leaf;

import io.grpc.stub.StreamObserver;
import org.seppiko.glf.api.Logger;
import org.seppiko.glf.api.LoggerFactory;
import org.seppiko.leaf.exceptions.LeafException;
import org.seppiko.leaf.proto.LeafMsg;
import org.seppiko.leaf.proto.LeafReq;
import org.seppiko.leaf.proto.LeafResp;
import org.seppiko.leaf.proto.LeafServiceGrpc;
import org.seppiko.snowflake.IdWorker;

/**
 * gRPC service implementation
 *
 * @author Leonard Woo
 */
public class LeafServiceImpl extends LeafServiceGrpc.LeafServiceImplBase {

    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private final long offsetTimestamp;

    /**
     * Initialization constructor
     *
     * @param offsetTimestamp UTC offset timestamp (millisecond)
     */
    public LeafServiceImpl(long offsetTimestamp) {
        this.offsetTimestamp = offsetTimestamp;
    }

    /**
     * Id Generator
     *
     * @param request Service ID and Node ID
     * @param responseObserver Response Stream
     */
    @Override
    public void idGen(LeafReq request, StreamObserver<LeafResp> responseObserver) {
        IdWorker sf = IdWorker.getInstance(offsetTimestamp);
        logger.debug(sf.toString());
        try {
            sf.initWorker(request.getServiceId(), request.getNodeId());
            responseObserver.onNext(LeafResp.newBuilder().setId(sf.nextId()).build());
        } catch (IllegalArgumentException | IllegalStateException ex) {
            logger.warn("Leaf ID generator failed.", new LeafException(ex));
            responseObserver.onError(ex);
        }
        responseObserver.onCompleted();
    }


    /**
     * Id Parse
     *
     * @param request Leaf ID
     * @param responseObserver Response Stream
     */
    @Override
    public void idPar(LeafResp request, StreamObserver<LeafMsg> responseObserver) {
        IdWorker sf = IdWorker.getInstance(offsetTimestamp);
        logger.debug(sf.toString());
        responseObserver.onNext(LeafMsg.newBuilder().setMessage(sf.formatId(request.getId())).build());
        responseObserver.onCompleted();
    }

}
