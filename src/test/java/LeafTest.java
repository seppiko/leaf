/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.time.Clock;
import java.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.seppiko.glf.api.Logger;
import org.seppiko.glf.api.LoggerFactory;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import org.seppiko.leaf.config.LeafConfiguration;
import org.seppiko.leaf.entity.LeafConfigEntity;
import org.seppiko.snowflake.IdWorker;

/**
 * @author Leonard Woo
 */

public class LeafTest {

    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private LeafConfigEntity config;

    @BeforeEach
    public void initTest() {
        config = LeafConfiguration.getInstance().getConfig();
    }

//    @Test
//    public void genTime() {
//        logger.info("Config: " + config.toString());
//        logger.info("UTC unix millisecond: " + Instant.now(Clock.systemUTC()).toEpochMilli() + "" );
//    }

    @Test
    public void genId() throws Exception {
        IdWorker idGenerator = IdWorker.getInstance(config.timeOffset());
        idGenerator.initWorker(1, 1);
        for (int i = 0; i < 10; i++) {
            logger.info("ID -> " + idGenerator.nextId() + " -> " + idGenerator.getConfig().getEntity().getSequence());
        }
    }

    @Test
    public void formatId() throws Exception {
        logger.info("Config offset timestamp: " + Instant.ofEpochMilli(config.timeOffset()) );
        IdWorker sf = IdWorker.getInstance(config.timeOffset());
        sf.initWorker(3, 5);
        logger.info("Time offset: " +  Instant.ofEpochMilli( sf.getConfig().getEntity().getTimestamp() ) );
        logger.info("Time now: " + Instant.now(Clock.systemUTC()) );
        logger.info(sf.toString());
        long id = sf.nextId();
        logger.info("ID: " + id + ", Length: " + String.valueOf(id).length());
        logger.info(sf.formatId(id));
    }

    @Test
    public void timeCalc() throws Exception {
        ZonedDateTime initOffset = ZonedDateTime.ofInstant(Instant.ofEpochMilli(config.timeOffset()), ZoneId.of("UTC"));
        logger.info("Now: " + Instant.ofEpochSecond(initOffset.toEpochSecond()).toString() );

        ZonedDateTime now = ZonedDateTime.now(Clock.systemUTC());
        Instant calc = Instant.ofEpochSecond( now.toEpochSecond() - initOffset.toEpochSecond());
        logger.info(">> " + calc.toString() );
    }
}
